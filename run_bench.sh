#!/bin/bash

export BLOCKSIZE=4k 
export RUNTIME=60
export OUTPUT=/home/ubuntu/results
export FILE=/dev/vdb

for x in randread randwrite
do
	for y in 1 2 4 8 16 32
	do
		for z in 1 2 4 8 16 32
		do
			sync
			echo "=== $FILE ==="
			echo "Running benchmark $x with I/O depth of $y and numjobs $z"
			export RW=$x
			export IODEPTH=$y
			export NUMJOBS=$z
			jobdescription=$'; -- start job file -
			[random-read]
			rw='$RW'
			blocksize='$BLOCKSIZE'
			filename='$FILE'
			ioengine=libaio
			iodepth='$IODEPTH'
			direct=1
			invalidate=1
			refill_buffers=1
			runtime='$RUNTIME'
			numjobs='$NUMJOBS'
			group_reporting=1
			write_bw_log=random-read-depth-'$IODEPTH'
			write_lat_log=random-read-depth-'$IODEPTH'
			write_iops_log=random-read-depth-'$IODEPTH'
			log_avg_msec=500

			; -- end job file --'
			echo "$jobdescription" > job
			sudo fio job --output-format=json > $OUTPUT/$(hostname)-$x-$y-$z.json   
		done
	done
done

